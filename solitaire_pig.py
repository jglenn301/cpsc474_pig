import sys

def get_value(values, base, turn, target):
    if base + turn >= target:
        return 0.0
    else:
        return values[base][turn]

    
def get_roll_value(values, base, turn, target):
    value = sum(1.0 / 6.0 * get_value(values, base, turn + i, target) for i in range(2, 7))
    value += 1.0 / 6.0 * (get_value(values, base, 0, target))
    return value


def solve_solitaire_pig(target, tolerance = 1e-08):
    values = [[0.0] * target for i in range(target)]

    max_delta = None
    iterations = 0
    evaluations = 0
    while max_delta is None or max_delta > tolerance:
        iterations += 1
        max_delta = 0.0
        for base in range(target):
            for turn in range(0, target - base):
                evaluations += 1
                old_value = values[base][turn]
                if turn == 0:
                    values[base][turn] = -1.0 + get_roll_value(values, base, turn, target)
                else:
                    roll_value = get_roll_value(values, base, turn, target)
                    bank_value = get_value(values, base + turn, 0, target)
                    values[base][turn] = max(roll_value, bank_value)
                    
                max_delta = max(max_delta, abs(old_value - values[base][turn]))

    policy = [[False] * (target - base) for base in range(target)]
    for base in range(target):
        for turn in range(target - base):
            policy[base][turn] = turn == 0 or get_roll_value(values, base, turn, target) > get_value(values, base + turn, 0, target)
                
    return policy, values, iterations, evaluations


policy, values, iterations, evals = solve_solitaire_pig(int(sys.argv[1]))
print(values[0][0])
print(iterations, "iterations;", evals, "evaluations")
